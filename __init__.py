import math

bl_info = {
		'name': 'Tau Editor',
		'author': 'Robert Fate <tehmoat@gmail.com>',
		'version': (0, 1, 0),
		'blender': (2, 7, 0),
		'description': 'Create and export Tau models and maps',
		'warning': '',
		'category': 'System'
}

import bpy
from mathutils import Vector

###
# Custom properties
###
tau_object_types = [
	'Map',
	'Model',
	'Collider',
	'Ref',
	'Dummy'
]

## Object props
bpy.types.Object.tau_type = bpy.props.EnumProperty(
	name='Object type',
	description='Object type',
	items=[(n, n, n) for n in tau_object_types],
	default='Dummy'
)

###
# Panels
###

class TauPanel():
	bl_label       = 'Tau'
	bl_space_type  = 'PROPERTIES'
	bl_region_type = 'WINDOW'

	def draw_header(self, context):
		self.layout.label(text='', icon='GAME')

	def draw(self, context):
		pass

def toggle_button(layout, obj, prop_name, true_text, false_text):
	text = true_text if getattr(obj, prop_name) else false_text
	layout.prop(obj, prop_name, text=text, toggle=True)

def debug_curve(box, curve):
	if len(curve.splines) > 1:
		error = 'Incorrect spline count (%s instead of 1)' % len(curve.splines)
		box.label(text=error, icon='ERROR')
		return

	spline = curve.splines[0]

	if spline.type != 'BEZIER':
		error = 'Incorrect spline type "%s" must be BEZIER' % spline.type
		box.label(text=error, icon='ERROR')
		return

	box.label(curve.name)
	box.label('%d points' % len(spline.bezier_points), icon='GROUP_VERTEX')

	row = box.row()
	row.prop(curve, 'dimensions', expand=True)

	row = box.row()
	row.prop(curve, 'resolution_u', text='Resolution', slider=True)
	toggle_button(row, spline, 'use_cyclic_u', 'Closed', 'Open')

class TauObjectPanel(TauPanel, bpy.types.Panel):
	bl_idname  = 'OBJECT_PT_tau'
	bl_context = 'object'

	@classmethod
	def poll(cls, context):
		return (context.object is not None)

	def draw(self, context):
		layout = self.layout
		box = layout.box()

		# Handle unambiguous bpy types first
		if context.object.type == 'CURVE':
			debug_curve(box, context.object.data)
		else:
			box.prop(context.object, 'tau_type', text='Type')

		if context.object.tau_type == 'Ref':
			if context.object.proxy is None:
				box.label(text='Object is not proxy!', icon='ERROR')
				return

			box.prop(context.object, 'locked')

			row = box.row()
			row.alignment = 'LEFT'
			row.label(text='Origin:')
			row.label(text=context.object.proxy.library.filepath)

		if context.object.tau_type == 'Map':
			box.label(text='test')

		if context.object.tau_type == 'Collider':
			if context.object.parent == None:
				row = box.row()
				row.label(text='Missing parent!', icon='ERROR')
				row.prop(context.object, 'parent', text='Parent')

		if context.object.tau_type == 'Model':
			box.label(text='test')

class TauMaterialPanel(TauPanel, bpy.types.Panel):
	bl_idname  = 'MATERIAL_PT_tau'
	bl_context = 'material'

	@classmethod
	def poll(cls, context):
		return (context.object is not None and context.object.active_material is not None)

	def draw(self, context):
		obj    = context.object
		mat    = obj.active_material
		layout = self.layout
		box    = layout.box()

		if mat.active_texture is None:
			row = box.row()
			row.label(text='Material has no assigned texture!', icon='ERROR')
			return

		box.label(text=mat.name)

###
# ballin
###

def register():
	bpy.utils.register_module(__name__)

	kc = bpy.context.window_manager.keyconfigs.addon
	if kc:
		km = kc.keymaps.new(name='3D View', space_type='VIEW_3D')
		kmi = km.keymap_items.new('tau.make_proxy', 'P', 'PRESS', ctrl=True, alt=True)

def unregister():
	bpy.utils.unregister_module(__name__)

if __name__ == '__main__':
	register()
